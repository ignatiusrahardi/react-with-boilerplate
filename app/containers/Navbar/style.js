import styled from 'styled-components';
import { NavLink, Link } from 'react-router-dom';

const Text = styled.div`
  margin-top: 3px;
`;

const TheNavbar = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  background: #20232a;
  color: #fff;
  height: 50px;
  padding: 10px;
`;

const Icon = styled.img`
  height: 30px;
  margin-right: 5px;
  margin-top: 4px;
`;

const Nav = styled.nav`
  display: flex;
  height: 100%;
  align-items: center;
`;

const Items = styled.div`
  text-decoration: none;
`;

const Spacer = styled.div`
  flex: 1;
`;

const Logo = styled(Link)`
  text-decoration: none;
  color: #61dafb;
  padding: 0 5px;
  font-size: 25px;
  font-weight: 700;
  padding-right: 10px;
  display: flex;
`;

const Page = styled(NavLink)`
  text-decoration: none;
  color: white;
  padding: 0px 5px;
  &:hover: {
    color: lightgrey;
  }
`;

export { Text, TheNavbar, Icon, Nav, Items, Spacer, Logo, Page };
