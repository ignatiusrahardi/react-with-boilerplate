/*
 * Navbar Messages
 *
 * This contains all the text for the Navbar container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.Navbar';

export default defineMessages({
  logo: {
    id: `${scope}.logo`,
    defaultMessage: 'React',
  },
  increment: {
    id: `${scope}.increment`,
    defaultMessage: 'Increment',
  },
  steam: {
    id: `${scope}.steam`,
    defaultMessage: 'Fetch Data',
  },
});
