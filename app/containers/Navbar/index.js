/**
 *
 * Navbar
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import makeSelectNavbar from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';

import { Text, TheNavbar, Nav, Items, Spacer, Logo, Page } from './style';

function Navbar() {
  useInjectReducer({ key: 'navbar', reducer });
  useInjectSaga({ key: 'navbar', saga });

  return (
    <TheNavbar>
      <Nav>
        <Logo to="/">
          <Text>
            <FormattedMessage {...messages.logo} />
          </Text>
        </Logo>
        <Spacer />
        <Items>
          <Page
            className="navLink"
            to="/calculator"
            activeStyle={{ color: 'gold' }}
          >
            <FormattedMessage {...messages.increment} />
          </Page>
          <Page
            className="navLink"
            to={{ pathname: '/steam' }}
            activeStyle={{ color: 'gold' }}
          >
            <FormattedMessage {...messages.steam} />
          </Page>
        </Items>
      </Nav>
    </TheNavbar>
  );
}

Navbar.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  navbar: makeSelectNavbar(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps,
);

export default compose(withConnect)(Navbar);
